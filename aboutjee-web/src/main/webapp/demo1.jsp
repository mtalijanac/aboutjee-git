<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>AboutJEE first demo</title>

    <c:url value="/webjars/jquery/1.12.0/jquery.min.js" var="jqueryUrl" />
    <script type="text/javascript" src="${jqueryUrl}"></script>

    <c:url value="/webjars/jquery-ui/1.12.1/jquery-ui.min.js" var="jqueryuiUrl" />
    <script type="text/javascript" src="${jqueryuiUrl}"></script>

    <c:url value="/webjars/jquery-ui/1.12.1/jquery-ui.min.css" var="jqueryuiThemeUrl" />
    <link href="${jqueryuiThemeUrl}" rel="stylesheet" type="text/css" />

    <c:url value="/webjars/jTable/2.4.0/themes/metro/blue/jtable.min.css" var="jtableThemeUrl" />
    <link href="${jtableThemeUrl}" rel="stylesheet" type="text/css" />

    <c:url value="/webjars/jTable/2.4.0/jquery.jtable.min.js" var="jtableUrl" />
    <script type="text/javascript" src="${jtableUrl}"></script>

    <c:url value="/first/1.0/load"   var="listActionUrl" />
    <c:url value="/first/1.0/create" var="createActionUrl" />
    <c:url value="/first/1.0/update" var="updateActionUrl" />
    <c:url value="/first/1.0/delete" var="deleteActionUrl" />

    <script type="text/javascript">
        $(document).ready(function () {
            $('#aboutJEEContainer').jtable({
                title: 'AboutJEE - First demo',
                actions: {
                    listAction:   '${listActionUrl}',
                    createAction: '${createActionUrl}',
                    updateAction: '${updateActionUrl}',
                    deleteAction: '${deleteActionUrl}'
                },
                fields: {
                    preferenceName: {
                        title: 'Preference Name',
                        width: '40%',
                        edit: false,
                        key: true
                    },
                    status: {
                        title: 'Status',
                        width: '20%'
                    },
                    type: {
                        title: 'Type',
                        width: '20%'
                    },
                    value: {
                        title: 'Value',
                        width: '30%'
                    }
                }
            });

            $('#aboutJEEContainer').jtable('load');
        });
    </script>

</head>
<body>

    <div id="aboutJEEContainer"></div>

    <a href="${jqueryUrl}">jquery.min.js</a>
    <a href="${jqueryuiUrl}">jquery-ui.min.js</a>
    <a href="${jqueryuiThemeUrl}">jquery-ui.min.css</a>
    <a href="${jtableThemeUrl}">jtable.min.css</a>
    <a href="${jtableUrl}">jtable.min.js</a>
    <a href="${listActionUrl}">listActionUrl</a>

</body>
</html>