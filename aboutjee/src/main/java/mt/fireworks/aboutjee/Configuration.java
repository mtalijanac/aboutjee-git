package mt.fireworks.aboutjee;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.Stack;

public class Configuration {

    final LinkedHashMap<String, Deque<Entry>> entries = new LinkedHashMap<String, Deque<Entry>>();

    public void add(Entry entry) {
        String key = entry.getPreferenceName();
        Deque<Entry> q = entries.get(key);
        if (q == null) {
            q = new ArrayDeque<Entry>();
            entries.put(key, q);
        }
        q.push(entry);
    }

    public void addAll(List<Entry> entries) {
        if (entries == null) {
            return;
        }
        for (Entry e: entries) {
            add(e);
        }
    }

    public Entry get(String key) {
        Deque<Entry> q = entries.get(key);
        return q == null ? null : q.peek();
    }

    public String val(String key) {
        Entry entry = get(key);
        return entry == null ? null : entry.getValue();
    }

    public String def(String key) {
        Deque<Entry> q = entries.get(key);
        if (q == null) {
            return null;
        }
        return q.isEmpty() ? null : q.peekLast().getValue();
    }

}
