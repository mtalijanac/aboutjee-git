package mt.fireworks.aboutjee;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/1.0")
public class AboutJEE  {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String root() {
        return "Welcome to AboutJEE instsnce";
    }

    @POST
    @Path("/load")
    @Produces(MediaType.APPLICATION_JSON)
    public String load() throws JsonProcessingException {
        Map res = new LinkedHashMap();
        res.put("Result", "OK");

        Entry e1 = new Entry();
        e1.setPreferenceName("example.entry");
        e1.setStatus("default");
        e1.setType("string");
        e1.setValue("one");

        Entry e2 = new Entry();
        e2.setPreferenceName("example.entry");
        e2.setStatus("default");
        e2.setType("string");
        e2.setValue("two");

        Entry e3 = new Entry();
        e3.setPreferenceName("example.entry");
        e3.setStatus("default");
        e3.setType("string");
        e3.setValue("three");

        ArrayList<Entry> entries = new ArrayList<Entry>();
        entries.add(e1);
        entries.add(e2);
        entries.add(e3);

        res.put("Records", entries);

        ObjectMapper mapper = new ObjectMapper();
        String val = mapper.writeValueAsString(res);
        return val;
    }


    @POST
    @Path("/create")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public String create(
        @FormParam("preferenceName") String preferenceName,
        @FormParam("type") String type,
        @FormParam("status") String status,
        @FormParam("value") String value
   ) throws JsonProcessingException {

        Entry e1 = new Entry();
        e1.setPreferenceName(preferenceName);
        e1.setStatus(status);
        e1.setType(type);
        e1.setValue(value);

        Map res = new LinkedHashMap();
        res.put("Result", "OK");
        res.put("Record", e1);

        ObjectMapper mapper = new ObjectMapper();
        String val = mapper.writeValueAsString(res);
        return val;
    }

    @POST
    @Path("/update")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public String update(
        @FormParam("preferenceName") String preferenceName,
        @FormParam("type") String type,
        @FormParam("status") String status,
        @FormParam("value") String value
    ) throws JsonProcessingException {

        Map res = new LinkedHashMap();
        res.put("Result", "OK");

        ObjectMapper mapper = new ObjectMapper();
        String val = mapper.writeValueAsString(res);
        return val;
    }

    @POST
    @Path("/delete")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public String delete(
        @FormParam("preferenceName") String preferenceName
    ) throws JsonProcessingException {
        Map res = new LinkedHashMap();
        res.put("Result", "OK");

        ObjectMapper mapper = new ObjectMapper();
        String val = mapper.writeValueAsString(res);
        return val;
    }


}
