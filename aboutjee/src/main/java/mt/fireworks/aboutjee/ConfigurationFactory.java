package mt.fireworks.aboutjee;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import lombok.Cleanup;
import lombok.Setter;


public class ConfigurationFactory {

    @Setter List<String> resourceStrings = new ArrayList<String>();

    public Configuration getConfiguration() throws IOException {
        Configuration conf = new Configuration();
        for (String res: resourceStrings) {
            List<Entry> entries = null;
            if (res.startsWith("classpath:") && res.endsWith(".properties")) {
                entries = loadClasspathProperties(res);
            }
            else if (res.startsWith("file:") && res.endsWith(".properties")) {
                entries = loadClasspathProperties(res);
            }
            conf.addAll(entries);
        }

        return conf;
    }

    List<Entry> loadClasspathProperties(String resourceString) throws IOException {
        String path = resourceString.replace("classpath:", "");
        @Cleanup InputStream is = this.getClass().getClassLoader().getResourceAsStream(path);
        if (is == null) {
            return null;
        }

        Properties p = new Properties();
        p.load(is);
        return fromProperties(p);
    }

    List<Entry> loadFileProperties(String resourceString) throws IOException {
        String path = resourceString.replace("file:", "");
        @Cleanup FileInputStream fis = new FileInputStream(path);
        if (fis == null) {
            return null;
        }

        Properties p = new Properties();
        p.load(fis);
        return fromProperties(p);
    }

    List<Entry> fromProperties(Properties properties) {
        List<Entry> entries = new ArrayList<Entry>();
        for (String key: properties.stringPropertyNames()) {
            String val = (String) properties.get(key);

            Entry e = new Entry();
            e.setPreferenceName(key);
            e.setValue(val);
            entries.add(e);
        }

        return entries;
    }
}
