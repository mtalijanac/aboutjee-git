package mt.fireworks.aboutjee;

import lombok.Data;

@Data
public class Entry {

    String preferenceName;
    String status;
    String type;
    String value;

    String source;

}
