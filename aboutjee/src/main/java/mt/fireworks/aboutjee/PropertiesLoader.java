package mt.fireworks.aboutjee;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class PropertiesLoader {

    public Set<Entry> parseProperties(Properties prop){
        Set<Entry> entries = new HashSet<Entry>();
        for (String name: prop.stringPropertyNames()) {
            String value = prop.getProperty(name);

            Entry e = new Entry();
            e.setPreferenceName(name);
            e.setValue(value);

            entries.add(e);
        }

        return entries;
    }

}
